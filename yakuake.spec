%global _changelog_trimtime %(date +%s -d "1 year ago")

Name:           yakuake
Version:        23.08.5
Release:        2
Summary:        A drop-down terminal emulator

# KDE e.V. may determine that future GPL versions are accepted
License:        GPLv2 or GPLv3
URL:            https://kde.org/applications/system/org.kde.yakuake

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{name}-%{version}.tar.xz

Requires:       konsole5-part

BuildRequires:  desktop-file-utils
BuildRequires:  extra-cmake-modules
BuildRequires:  gettext
BuildRequires:  kf5-rpm-macros
BuildRequires:  kf5-karchive-devel
BuildRequires:  kf5-kconfig-devel
BuildRequires:  kf5-kcoreaddons-devel
BuildRequires:  kf5-kcrash-devel
BuildRequires:  kf5-kdbusaddons-devel
BuildRequires:  kf5-kglobalaccel-devel
BuildRequires:  kf5-ki18n-devel
BuildRequires:  kf5-kiconthemes-devel
BuildRequires:  kf5-kio-devel
BuildRequires:  kf5-knewstuff-devel
BuildRequires:  kf5-knotifications-devel
BuildRequires:  kf5-knotifyconfig-devel
BuildRequires:  kf5-kparts-devel
BuildRequires:  kf5-kwidgetsaddons-devel
BuildRequires:  kf5-kwindowsystem-devel

BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5Svg)
BuildRequires:  pkgconfig(Qt5X11Extras)

BuildRequires:  cmake(KF5Wayland)

%global appstream_validate 1
BuildRequires:  libappstream-glib

%description
Yakuake is a drop-down terminal emulator.


%prep
%autosetup -p1


%build
%{cmake_kf5}
%cmake_build
 


%install
%cmake_install

%find_lang %{name}


%check
%if 0%{?appstream_validate}
appstream-util validate-relax --nonet %{buildroot}%{_kf5_metainfodir}/org.kde.yakuake.appdata.xml
%endif
desktop-file-validate  %{buildroot}%{_kf5_datadir}/applications/org.kde.yakuake.desktop


%files -f %{name}.lang
%doc AUTHORS ChangeLog TODO
%license LICENSES/*
%{_kf5_bindir}/yakuake
%{_kf5_datadir}/knsrcfiles/yakuake.knsrc
%{_kf5_metainfodir}/org.kde.yakuake.appdata.xml
%{_kf5_datadir}/applications/org.kde.yakuake.desktop
%{_kf5_datadir}/knotifications5/yakuake.notifyrc
%{_kf5_datadir}/yakuake/
%{_kf5_datadir}/icons/hicolor/*/apps/yakuake.*
%{_kf5_datadir}/dbus-1/services/org.kde.yakuake.service


%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Tue Jan 02 2024 lvgenggeng<lvgenggeng@uniontech.com> - 23.08.4-1
- bump to 23.08.4

* Fri Aug 04 2023 yajun<yajun@kylinos.cn> - 23.04.3-1
- update to upstream version 23.04.3

* Tue Dec 13 2022 lijian <lijian2@kylinos.cn> - 22.12.0-1
- update to upstream version 22.12.0

* Tue Jul 12 2022 peijiankang<peijiankang@kylinos.cn> - 22.04.2-2
- fix license error

* Tue Jul 5 2022 peijiankang<peijiankang@kylinos.cn> - 22.04.2-1
- update to upstream version 22.04.2

* Sat Mar 26 2022 huayadong<huayadong@kylinos.cn> - 21.12.2-2
- 修改 yakuake.spec文件 modify spec

* Sat Feb 12 2022 peijiankang<peijiankang@kylinos.cn> - 21.12.2-1
- update to upstream version 21.12.2

* Mon Jan 24 2022 peijiankang<peijiankang@kylinos.com> - 21.08.3-1
- update to upstream version 21.08.3
